package Gebeya;

public interface Observer {
	
	public void update(double teff, double banana, double shiro);
	
}