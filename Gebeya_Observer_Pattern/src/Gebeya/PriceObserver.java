package Gebeya;

//Represents each Observer that is monitoring changes in the subject

public class PriceObserver implements Observer {
	
	private double teffPrice;
	private double bananaPrice;
	private double shiroPrice;
	
	// Static used as a counter
	
	private static int observerIDTracker = 0;
	
	// Used to track the observers
	
	private int observerID;
	
	// Will hold reference to the Price Notifier object
	
	private Subject priceNotifier;
	
	public PriceObserver(Subject priceNotifier){
		
		// Store the reference to the priceNotifier object so
		// I can make calls to its methods
		
		this.priceNotifier = priceNotifier;
		
		// Assign an observer ID and increment the static counter
		
		this.observerID = ++observerIDTracker;
		
		// Message notifies user of new observer
		
		System.out.println("New Observer " + this.observerID);
		
		// Add the observer to the Subjects ArrayList
		
		priceNotifier.register(this);
		
	}
	
	// Called to update all observers
	
	public void update(double teffPrice, double bananaPrice, double shiroPrice) {
		
		this.teffPrice = teffPrice;
		this.bananaPrice = bananaPrice;
		this.shiroPrice = shiroPrice;
		
		printThePrices();
		
	}
	
	public void printThePrices(){
		
		System.out.println(observerID + "\nTEFF: " + teffPrice + "\nBANANA: " + 
				bananaPrice + "\nSHIRO: " + shiroPrice + "\n");
		
	}
	
}