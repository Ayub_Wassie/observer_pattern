package Gebeya;

import java.util.ArrayList;

//Uses the Subject interface to update all Observers

public class PriceNotifier implements Subject{
	
	private ArrayList<Observer> observers;
	private double teffPrice;
	private double bananaPrice;
	private double shiroPrice;
	
	public PriceNotifier(){
		
		// Creates an ArrayList to hold all observers
		
		observers = new ArrayList<Observer>();
	}
	
	public void register(Observer newObserver) {
		
		// Adds a new observer to the ArrayList
		
		observers.add(newObserver);
		
	}

	public void unregister(Observer deleteObserver) {
		
		// Get the index of the observer to delete
		
		int observerIndex = observers.indexOf(deleteObserver);
		
		// Print out message (Have to increment index to match)
		
		System.out.println("Observer " + (observerIndex+1) + " deleted");
		
		// Removes observer from the ArrayList
		
		observers.remove(observerIndex);
		
	}

	public void notifyObserver() {
		
		// Cycle through all observers and notifies them of
		// price changes
		
		for(Observer observer : observers){
			
			observer.update(teffPrice, bananaPrice, shiroPrice);
			
		}
	}
	
	// Change prices for all stocks and notifies observers of changes
	
	public void setTeffPrice(double newTeffPrice){
		
		this.teffPrice = newTeffPrice;
		
		notifyObserver();
		
	}
	
	public void setBananaPrice(double newBananaPrice){
		
		this.bananaPrice = newBananaPrice;
		
		notifyObserver();
		
	}

	public void setShiroPrice(double newShiroPrice){
	
		this.shiroPrice = newShiroPrice;
	
		notifyObserver();
	
	}
	
}