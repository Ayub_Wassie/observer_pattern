package Gebeya;

public class Grab_Items {

	public static void main(String[] args){
		
		// Create the Subject object
		// It will handle updating all observers 
		// as well as deleting and adding them
		
		PriceNotifier priceNotifier = new PriceNotifier();
		
		// Create an Observer that will be sent updates from Subject
		
		PriceObserver observer1 = new PriceObserver(priceNotifier);
		
		priceNotifier.setTeffPrice(197.00);
		priceNotifier.setBananaPrice(677.60);
		priceNotifier.setShiroPrice(676.40);
		
		PriceObserver observer2 = new PriceObserver(priceNotifier);
		
		priceNotifier.setTeffPrice(200.00);
		priceNotifier.setBananaPrice(300.60);
		priceNotifier.setShiroPrice(400.40);
		
		// Delete one of the observers
		
		priceNotifier.unregister(observer1);
		
		priceNotifier.setTeffPrice(500.00);
		priceNotifier.setBananaPrice(600.60);
		priceNotifier.setShiroPrice(700.40);
		

		
	}
}
